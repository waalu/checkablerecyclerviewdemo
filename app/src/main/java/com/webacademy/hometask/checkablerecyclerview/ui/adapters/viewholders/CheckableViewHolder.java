package com.webacademy.hometask.checkablerecyclerview.ui.adapters.viewholders;


import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;

import com.webacademy.hometask.checkablerecyclerview.ui.views.CheckableView;

public class CheckableViewHolder extends RecyclerView.ViewHolder {

    public interface Callback {

        void check(int id, boolean isChecked);

    }

    private int mId;
    private CheckableView mView;
    private Callback mExternalCallback;

    private CheckableView.OnCheckedChangeListener mInternalCheckListener = new CheckableView.OnCheckedChangeListener() {

        @Override
        public void onCheckedChanged(boolean isChecked) {
            if(mExternalCallback!=null){
                mExternalCallback.check(mId,isChecked);
            }
        }
    };


    public CheckableViewHolder(@NonNull Callback callback,@NonNull CheckableView itemView) {
        super(itemView);
        mExternalCallback = callback;
        mView = itemView;
        mView.setOnCheckedChangeListener(mInternalCheckListener);
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    public void setValue(String value) {
        mView.setValue(value);
    }

    public void setChecked(boolean isChecked) {
        mView.setChecked(isChecked);
    }

    public void setCheckable(boolean isCheckable) {
        mView.setCheckable(isCheckable);
    }


}

