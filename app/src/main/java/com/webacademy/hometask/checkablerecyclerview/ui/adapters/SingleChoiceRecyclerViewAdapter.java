package com.webacademy.hometask.checkablerecyclerview.ui.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.webacademy.hometask.checkablerecyclerview.R;
import com.webacademy.hometask.checkablerecyclerview.ui.adapters.viewholders.CheckableViewHolder;
import com.webacademy.hometask.checkablerecyclerview.ui.views.CheckableView;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;


public class SingleChoiceRecyclerViewAdapter extends RecyclerView.Adapter<CheckableViewHolder> {

    private static final String TAG = SingleChoiceRecyclerViewAdapter.class.getSimpleName();

    private static final String PARAM_ITEMS_ARRAY_LIST = "PARAM_ITEMS_ARRAY_LIST";


    private final SparseBooleanArray mSelectedItemsArray;
    private final List<String> mItemsArrayList;
    private final LayoutInflater mLayoutInflater;
    private ChoiceListener mExternalChoiceListener;
    private boolean isActionModeActivated;

    public interface ChoiceListener {

        void updateSelectedItemsCount(int count);

        void updateTotalItemsCount(int count);

    }

    private ChoiceListener mInternalChoiceListener = new ChoiceListener() {
        @Override
        public void updateSelectedItemsCount(int count) {
            if (mExternalChoiceListener != null) {
                mExternalChoiceListener.updateSelectedItemsCount(count);
            }
        }

        @Override
        public void updateTotalItemsCount(int count) {
            if (mExternalChoiceListener != null) {
                mExternalChoiceListener.updateTotalItemsCount(count);
            }
        }
    };

    private CheckableViewHolder.Callback mViewHolderCallback = new CheckableViewHolder.Callback() {

        @Override
        public void check(int id, boolean isChecked) {
            if (isChecked) {
                mSelectedItemsArray.put(id, true);
            } else {
                mSelectedItemsArray.delete(id);
            }
            mInternalChoiceListener.updateSelectedItemsCount(mSelectedItemsArray.size());
        }
    };


    public SingleChoiceRecyclerViewAdapter(Context context) {
        this.mSelectedItemsArray = new SparseBooleanArray();
        this.mItemsArrayList = new ArrayList<String>();
        this.mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public CheckableViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CheckableView view = (CheckableView) mLayoutInflater.inflate(R.layout.checkable_recycler_view_item_layout, parent, false);
        return new CheckableViewHolder(mViewHolderCallback,view);
    }

    @Override
    public void onBindViewHolder(CheckableViewHolder holder, int position) {
        holder.setId(position);
        holder.setValue(mItemsArrayList.get(position));
        holder.setCheckable(isActionModeActivated);
        holder.setChecked(mSelectedItemsArray.get(position, false));
    }

    @Override
    public int getItemCount() {
        return mItemsArrayList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setActionMode(boolean isEnabled) {
        isActionModeActivated = isEnabled;
        notifyDataSetChanged();
        clearSelection();
    }


    public void addItem(String item) {
        mItemsArrayList.add(item);
        mInternalChoiceListener.updateTotalItemsCount(mItemsArrayList.size());
        notifyItemInserted(mItemsArrayList.size());
    }

    public void clearSelection() {
        mSelectedItemsArray.clear();
        refreshCounters();
        notifyDataSetChanged();
    }

    private void refreshCounters() {
        mInternalChoiceListener.updateSelectedItemsCount(mSelectedItemsArray.size());
        mInternalChoiceListener.updateTotalItemsCount(mItemsArrayList.size());
    }


    public void selectAll() {
        int size = mItemsArrayList.size();
        for (int i = 0; i < size; i++) {
            mSelectedItemsArray.put(i, true);
        }
        refreshCounters();
        notifyDataSetChanged();
    }



    public void removeSelectedItems() {
        int index = 0;
        ListIterator<String> iterator = mItemsArrayList.listIterator();
        while (iterator.hasNext()) {
            iterator.next();
            if (mSelectedItemsArray.indexOfKey(index) >= 0) {
                iterator.remove();
            }
            index++;
        }
        mSelectedItemsArray.clear();
        refreshCounters();
        notifyDataSetChanged();
    }

    public void setChoiceListener(ChoiceListener choiceListener) {
        this.mExternalChoiceListener = choiceListener;
        refreshCounters();
    }

    public void removeChoiceListener(){
        this.mExternalChoiceListener = null;
    }


    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putStringArrayList(PARAM_ITEMS_ARRAY_LIST, (ArrayList<String>) mItemsArrayList);
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        if(savedInstanceState!=null) {
            List<String> savedStateList = savedInstanceState.getStringArrayList(PARAM_ITEMS_ARRAY_LIST);
            if (savedStateList != null && !savedStateList.isEmpty()) {
                mItemsArrayList.addAll(savedStateList);
            }
        }
    }




}
